//
//  LoginViewController.swift
//  TravelApp
//
//  Created by Nikita on 22.05.2021.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInDelegate {

    
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
        GIDSignIn.sharedInstance()?.delegate = self

    }
    
    @IBAction func eyeButtonClicked(_ sender: Any) {
        
        if eyeButton.isSelected {
            eyeButton.isSelected = false
            passwordTextField.isSecureTextEntry = false
        } else {
            eyeButton.isSelected = true
            passwordTextField.isSecureTextEntry = true
        }
 
    }
    
    
    @IBAction func loginClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            //  сервис firebase
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    
                    let userId = Auth.auth().currentUser?.uid
                    
                    ServerManager.shared.downloadArtistsFromFirebase(for: userId!)
                    ServerManager.shared.downloadTracksFromFirebase(for: userId!)
                    
                    
                    let tabbar: UITabBarController? = (self.storyboard!.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController)
//                    print("navigationController = \(self.navigationController)")
                    self.navigationController?.pushViewController(tabbar!, animated: true)
                   

//
//                    if let topArtistsVC = self.storyboard?.instantiateViewController(identifier: "TopArtistsViewController") as? TopArtistsViewController {
//                        self.navigationController?.pushViewController(topArtistsVC, animated: true)
                    
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error" // если user nil то зайзет сюда
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert) // создали алерт
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil) // создали кнопку ОК
                    
                    alertVC.addAction(action) // добавили кнопку к алерту
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
    
   
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        return GIDSignIn.sharedInstance()!.handle(url)
    }
    
    // login через google
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if user != nil {
            if let profileVC = self.storyboard?.instantiateViewController(identifier: "TopArtistsViewController") as? TopArtistsViewController {
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        } else {
            let errorMessage = error?.localizedDescription ?? "Error" // если user nil то зайзет сюда
            let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert) // создали алерт
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil) // создали кнопку ОК
            
            alertVC.addAction(action) 
            self.present(alertVC, animated: true, completion: nil)
        }
    }
}

    

