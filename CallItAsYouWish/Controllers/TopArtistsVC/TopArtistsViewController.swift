//
//  FeedViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 17.06.2021.
//

import UIKit
import Alamofire
import Kingfisher
import FirebaseAuth
import RealmSwift

class TopArtistsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
   
    @IBOutlet weak var topArtistCollectionView: UICollectionView!

    
    var artists: [TopArtist] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       title = "Top Artists"

        Auth.auth().currentUser
        
       let cellXib = UINib(nibName: "TopArtistsViewCell", bundle: nil)
        topArtistCollectionView.register(cellXib, forCellWithReuseIdentifier: "TopArtistsViewCell")
        
       
        topArtistCollectionView.delegate = self
        topArtistCollectionView.dataSource = self
        
        let lastFMManager = LastFMManager()
        lastFMManager.getTopArtists { [weak self] artists in
            guard let self = self else {return}
            self.artists = artists
            self.topArtistCollectionView.reloadData()
        }
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.prefersLargeTitles = true
        topArtistCollectionView.reloadData()
    }
     
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        artists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let artistCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopArtistsViewCell", for: indexPath) as! TopArtistsViewCell
        artistCell.favouriteArtistOutlet.isSelected = false
        let artist = artists[indexPath.row]
        if let artistImage = artist.image?[2].url{
            let photo = URL(string: artistImage)
            artistCell.artistImage.kf.setImage(with: photo)
        }
        artistCell.artistName.text = artist.name
        
        // MARK:  Проверка на отметку, есть ли в реалме такой же артист и если есть, ставь отметку
        let realm = try! Realm()
        let favouriteArtist = realm.object(ofType: RealmFavoriteArtist.self, forPrimaryKey: artist.id)
        artistCell.favouriteArtistOutlet.isSelected = favouriteArtist != nil
        
        
        artistCell.addArtistToFavoriteClosure = { [weak self] in
            guard let self = self else { return }
            print("Был выбран артист \(indexPath.row)")
            let favouriteArtist = self.artists[indexPath.row]
            DataStorage.shared.favouriteArtists.append(favouriteArtist)
            print(DataStorage.shared.favouriteArtists)
            
            
            let userId = Auth.auth().currentUser?.uid

            let realm = try! Realm()
            let realmArtist = RealmArtistDB()
            realmArtist.id = favouriteArtist.id 
            realmArtist.userId = userId!
            realmArtist.name = favouriteArtist.name!
            realmArtist.numberOfListeners = favouriteArtist.numberOfListeners!
            realmArtist.numberOfPlay = favouriteArtist.numberOfPlay!
            realmArtist.url = favouriteArtist.url!
             
            if let images = favouriteArtist.image {
                for image in images {
                    let realmImage = RealmImageOption()
                    realmImage.url = image.url ?? ""
                    realmArtist.images.append(realmImage)
                }
            }
            
            let markForFavArtist = RealmFavoriteArtist() // id артиста добавляем отдельно для функции добавить в избранное 
            markForFavArtist.id = favouriteArtist.id
            
        
            try! realm.write {
                realm.add(realmArtist)
                realm.add(markForFavArtist)
            }
            
            ServerManager.shared.sendArtistToFirebase(realmArtist)
        }
        
        artistCell.removeArtistFromFavoriteClosure = { [weak self] in
            guard let self = self else { return }
            print("\(indexPath.row) была удалена из избранного")
            let unFavouriteArtist = self.artists[indexPath.row]
            let artistId = unFavouriteArtist.id
            DataStorage.shared.favouriteArtists.removeAll(where: {($0.id) == (unFavouriteArtist.id)})
            
            let realm = try! Realm()
            
            if let removeArtistFromRealm = realm.object(ofType: RealmArtistDB.self, forPrimaryKey: unFavouriteArtist.id),
               let removeArtistId = realm.object(ofType: RealmFavoriteArtist.self, forPrimaryKey: artistId){
                
                try! realm.write{
                    realm.delete(removeArtistFromRealm.images)
                    realm.delete(removeArtistFromRealm)
                    realm.delete(removeArtistId)
                }
            }
            ServerManager.shared.removeArtistFromFirebse(for: artistId)
        }
        
        return artistCell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let offSet: CGFloat = 20 // отступы слева справав
        let spacing: CGFloat = 10
        let screenWidth = UIScreen.main.bounds.width
        let cellWidth = (screenWidth - offSet*2 - spacing) / 2
        let size = CGSize(width: cellWidth, height: cellWidth)

        return size
    }

 

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    //расстояние по y(игрику) между элементами
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let topArtistVC = TopArtistViewController()
        let artist = artists[indexPath.row]
        topArtistVC.artist = artist
        
        self.navigationController?.pushViewController(topArtistVC, animated: true)
        
    }
    
    

}
