//
//  TopArtistViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 11.08.2021.
//

import UIKit

class TopArtistViewController: UIViewController {
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var numberOfPlays: UILabel!
    @IBOutlet weak var numberOfListeners: UILabel!
    @IBOutlet weak var checkOnLastFMLabel: UIButton!
    
    var artist: TopArtist?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.hidesBackButton = false

        if let artist = artist{
            artistName.text = artist.name
            numberOfPlays.text = ("Number of plays:  \(artist.numberOfPlay!)")
            numberOfListeners.text = ("Number of listeners:  \(artist.numberOfListeners!)")
            if let artistPhoto = artist.image?[3].url{
                let image = URL(string: artistPhoto)
                artistImage.kf.setImage(with: image)
            }
        }
       
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        checkOnLastFMLabel.addTarget(self, action: #selector(openLink), for: .touchUpInside)
    }

    @objc func openLink(){
        if let url = artist?.url{
            if let trackLink = URL(string: url){
                UIApplication.shared.open(trackLink, options: [:]) { done in
                    print("пользователь перешёл по ссылке")
                }
            }
        }
    }

    
   

}
