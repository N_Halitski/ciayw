//
//  TopArtistsViewCell.swift
//  CallItAsYouWish
//
//  Created by Nikita on 07.08.2021.
//

import UIKit
import RealmSwift

class TopArtistsViewCell: UICollectionViewCell {
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var favouriteArtistOutlet: UIButton!
    
    var addArtistToFavoriteClosure:(() -> Void)?
    var removeArtistFromFavoriteClosure:(() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func favouriteArtistClicked(_ sender: Any) {
        
        if favouriteArtistOutlet.isSelected {
            favouriteArtistOutlet.isSelected = false
            removeArtistFromFavoriteClosure?()
        } else {
            favouriteArtistOutlet.isSelected = true
            addArtistToFavoriteClosure?()
            
        }
        
    }
    
}
