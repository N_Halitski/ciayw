//
//  SearchTableViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 11.07.2021.
//

import UIKit
import Kingfisher
import RealmSwift
import FirebaseAuth
import EzPopup


class FavouriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    
    @IBOutlet var favouriteListTableView: UITableView!
    @IBOutlet weak var favouriteSegmentedControl: UISegmentedControl!
    
    var realmFavTracksList: Results<RealmTracksDB>!
    var realmFavArtistsList: Results<RealmArtistDB>!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().barTintColor = .darkGray
        
        let userId = Auth.auth().currentUser?.uid
        
        print(Realm.Configuration.defaultConfiguration.fileURL?.path)  // тут лежат треки и альбомы
        
        let realm = try! Realm()
        realmFavTracksList = realm.objects(RealmTracksDB.self)
        realmFavArtistsList = realm.objects(RealmArtistDB.self)
        
        
        
        if realmFavArtistsList.isEmpty && realmFavTracksList.isEmpty{
            showAlertButtonTapped()
        }
        
        self.navigationItem.title = "Favourites"
        
        let favouriteTracksXib = UINib(nibName: "FavouriteTrackCell", bundle: nil)
        favouriteListTableView.register(favouriteTracksXib, forCellReuseIdentifier: "FavouriteTrackCell")
        favouriteListTableView.delegate = self
        favouriteListTableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        favouriteListTableView.reloadData()
    }
    
    @IBAction func showAlertButtonTapped() {
        
        // create the alert
        let alert = UIAlertController(title: "Ooops", message: "You dont have any favourite tracks or artist yet!", preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Get some Tracks", style: UIAlertAction.Style.default, handler: { action in
            
            self.tabBarController?.selectedIndex = 1
        }))
        alert.addAction(UIAlertAction(title: "Get some Artist", style: UIAlertAction.Style.default, handler: { action in
            self.tabBarController?.selectedIndex = 0
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if favouriteSegmentedControl.selectedSegmentIndex == 0 {
            return realmFavTracksList.count
            
        } else {
            return realmFavArtistsList.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if favouriteSegmentedControl.selectedSegmentIndex == 0{
            let songLink = realmFavTracksList[indexPath.row].songLink
            
            let contentVC = PopUpViewController()
            contentVC.link = songLink
            
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: UIScreen.main.bounds .width * 0.7 , popupHeight: UIScreen.main.bounds.height / 4 )
            
            present(popupVC, animated: true)
            
        } else {
            let artistLink = realmFavArtistsList[indexPath.row].url
            
            let contentVC = PopUpViewController()
            contentVC.link = artistLink
            
            let popupVC = PopupViewController(contentController: contentVC, popupWidth: UIScreen.main.bounds .width * 0.7 , popupHeight: UIScreen.main.bounds.height / 4 )
            
            present(popupVC, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = favouriteListTableView.dequeueReusableCell(withIdentifier: "FavouriteTrackCell") as! FavouriteTrackCell
        cell.selectionStyle = .none
        if realmFavTracksList != nil && realmFavArtistsList != nil {
            
            if favouriteSegmentedControl.selectedSegmentIndex == 0{
                
                cell.artistNameForTrack.text = realmFavTracksList[indexPath.row].artist?.name
                cell.trackName.text = realmFavTracksList[indexPath.row].name
                if let photo = realmFavTracksList[indexPath.row].images?.smallImage{
                    let artistImage = URL(string: photo)
                    cell.trackImage.kf.setImage(with: artistImage)}
                
            } else {
                cell.artistNameForTrack.text = ("Number of plays: \(realmFavArtistsList[indexPath.row].numberOfPlay)")
                cell.trackName.text = realmFavArtistsList[indexPath.row].name
                if let artistPhoto = realmFavArtistsList[indexPath.row].images[0].url{
                    let artistImage = URL(string: artistPhoto)
                    cell.trackImage.kf.setImage(with: artistImage)
                }
                
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            if favouriteSegmentedControl.selectedSegmentIndex == 0 {
                
                let trackToDelete = realmFavTracksList[indexPath.row]
                let trackIdToDelete = realmFavTracksList[indexPath.row].id
                
                let realm = try! Realm()
                
                if let removeTrackFromRealm = realm.object(ofType: RealmTracksDB.self, forPrimaryKey: trackToDelete.id),
                   let removeSongId = realm.object(ofType: RealmFavoriteTrack.self, forPrimaryKey: trackIdToDelete){
                    
                    try! realm.write{
                        if let imagesForTrack = removeTrackFromRealm.images,
                           let artistForTrack = removeTrackFromRealm.artist{
                            
                            realm.delete(imagesForTrack)
                            realm.delete(artistForTrack)
                        }
                        
                        realm.delete(removeTrackFromRealm)
                        realm.delete(removeSongId)
                    }
                    
                }
                self.favouriteListTableView.deleteRows(at: [indexPath], with: .automatic)
                ServerManager.shared.removeTrackFromFirebase(for: trackIdToDelete)
            } else {
                let artistToDelete = realmFavArtistsList[indexPath.row]
                let artistIdToDelete = realmFavArtistsList[indexPath.row].id
                
                
                
                let realm = try! Realm()
                
                if let removeArtistFromRealm = realm.object(ofType: RealmArtistDB.self, forPrimaryKey: artistToDelete.id),
                   let removeArtistId = realm.object(ofType: RealmFavoriteArtist.self, forPrimaryKey: artistIdToDelete){
                    
                    try! realm.write{
                        realm.delete(removeArtistFromRealm.images)
                        realm.delete(removeArtistFromRealm)
                        realm.delete(removeArtistId)
                    }
                }
                self.favouriteListTableView.deleteRows(at: [indexPath], with: .automatic)
                ServerManager.shared.removeArtistFromFirebse(for: artistIdToDelete)
                
            }
        }
    }
    
    
    
    @IBAction func segmentedControlClicked(_ sender: Any) {
        favouriteListTableView.reloadData()
    }
    
    
    
}
