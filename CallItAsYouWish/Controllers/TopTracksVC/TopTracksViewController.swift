//
//  CameraViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 06.07.2021.
//

import UIKit
import Kingfisher
import RealmSwift
import FirebaseAuth
import FirebaseDatabase

class TopTracksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tracks: [Track] = []
    let serverManager = ServerManager()
    var favoriteTracks: [Track] = []
    var realmFavTracksIDs: Results<RealmFavoriteTrack>!
    
    @IBOutlet weak var topTracksTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("realm file:\(Realm.Configuration.defaultConfiguration.fileURL?.path)")
        
        
        let lastFMManager = LastFMManager()
        lastFMManager.getTopTracks { [weak self] tracks in
            guard let self = self else {return}
            self.tracks = tracks
            self.topTracksTableView.reloadData()
            
        }
        
        let topTrackXib = UINib(nibName: "TopTrackCell", bundle: nil)
        topTracksTableView.register(topTrackXib, forCellReuseIdentifier: "TopTrackCell")
        
        topTracksTableView.delegate = self
        topTracksTableView.dataSource = self
        
        
        title = "Top Tracks"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.prefersLargeTitles = true
        topTracksTableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = topTracksTableView.dequeueReusableCell(withIdentifier: "TopTrackCell") as! TopTrackCell
        cell.selectionStyle = .none
        cell.favoriteButtonLabel.isSelected = false
        let track = tracks[indexPath.row]
        if let artistName = track.artist?.name{
            cell.trackArtistNameLabel.text = artistName
        }
        cell.trackNameLabel.text = track.name
        if let photo = track.image?.smallImage{
            let artistImage = URL(string: photo)
            cell.trackImage.kf.setImage(with: artistImage)
        }
        
        
        // MARK: здесь я хочу сравнивать id песен в таблице и id добавленных ранне песен в реалм, чтобы отобразить понравившиеся
        
        
        let realm = try! Realm()
        let favoriteTrack = realm.object(ofType: RealmFavoriteTrack.self, forPrimaryKey: track.id) // забирает объект если у него такой же айди как у трека в ячейке
        cell.favoriteButtonLabel.isSelected = favoriteTrack != nil
        
        
        
        cell.addSongToFavoriteClosure = { [weak self] in
            guard let self = self else { return }
            print("Была выбрана песня \(indexPath.row)")
            let favouriteSong = self.tracks[indexPath.row]
            DataStorage.shared.favouriteTracks.append(favouriteSong)
            
            
            let userId = Auth.auth().currentUser?.uid
            
            let realmTrack = RealmTracksDB()
            realmTrack.id = favouriteSong.id
            realmTrack.userId = userId!
            realmTrack.name = favouriteSong.name!
            realmTrack.playCounts = favouriteSong.playCounts!
            realmTrack.numberOfListeners = favouriteSong.numberOfListeners!
            realmTrack.songLink = favouriteSong.songLink!
            realmTrack.artist = RealmArtistForTrack()
            realmTrack.artist?.name = (favouriteSong.artist?.name!)!
            realmTrack.artist?.url = (favouriteSong.artist?.url!)!
            if let images = favouriteSong.image{
                var imagesForTrack = RealmTrackImage()
                imagesForTrack.smallImage = images.smallImage ?? ""
                imagesForTrack.mediumImage = images.mediumImage ?? ""
                realmTrack.images = imagesForTrack
            }
            
            let markForFavTrack = RealmFavoriteTrack()
            markForFavTrack.id = favouriteSong.id
            
            
            let realm = try! Realm()
            
            try! realm.write{
                realm.add(realmTrack, update: .all)
                realm.add(markForFavTrack)
            }
            
            ServerManager.shared.sendTrackToFirebase(realmTrack)
            
        }
        
        cell.removeSongFromFavoriteClosure = { [weak self] in
            guard let self = self else { return }
            print("\(indexPath.row) была удалена из избранного")
            let unFavouriteSong = self.tracks[indexPath.row]
            let trackId = self.tracks[indexPath.row].id
            
            let realm = try! Realm()
            
            if let removeTrackFromRealm = realm.object(ofType: RealmTracksDB.self, forPrimaryKey: unFavouriteSong.id),
               let removeSongId = realm.object(ofType: RealmFavoriteTrack.self, forPrimaryKey: trackId){
                
                try! realm.write{
                    if let imagesForTrack = removeTrackFromRealm.images,
                       let artistForTrack = removeTrackFromRealm.artist{
                        
                        realm.delete(imagesForTrack)
                        realm.delete(artistForTrack)
                    }
                    
                    realm.delete(removeTrackFromRealm)
                    realm.delete(removeSongId)
                }
 
            ServerManager.shared.removeTrackFromFirebase(for: trackId)
                
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let oneTrackVC = OneTrackViewController()
        let track = tracks[indexPath.row]
        oneTrackVC.track = track
        
        self.navigationController?.pushViewController(oneTrackVC, animated: true)
        // не забыть про navigation controller!!! в сториборде
    }
    
}
