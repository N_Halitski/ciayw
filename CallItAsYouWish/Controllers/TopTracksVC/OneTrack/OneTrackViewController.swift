//
//  OneTrackViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 04.08.2021.
//

import UIKit
import Kingfisher

class OneTrackViewController: UIViewController {
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var numberOfListeners: UILabel!
    
    @IBOutlet weak var listenOnLFMLabel: UIButton!
 

    var track: Track?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        listenOnLFMLabel.addTarget(self, action: #selector(openLink), for: .touchUpInside)
        
        
        artistName.text = track?.artist?.name
        trackName.text = track?.name
        if let trackListeners = track?.numberOfListeners{
            numberOfListeners.text = ("Number of listeners: \(trackListeners)")
        }
       
        if let artistPhoto = track?.image{
            let image = URL(string: artistPhoto.mediumImage!)
            artistImage.kf.setImage(with: image)
        }
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func openLink(){
        if let url = track?.songLink {
            if let trackLink = URL(string: url){
                UIApplication.shared.open(trackLink, options: [:]) { done in
                    print("пользователь перешёл по ссылке")
                }
            }
        }
    }
    
}
