//
//  TopTrackCell.swift
//  CallItAsYouWish
//
//  Created by Nikita on 03.08.2021.
//

import UIKit
import RealmSwift

class TopTrackCell: UITableViewCell {
    
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var trackArtistNameLabel: UILabel!
    @IBOutlet weak var favoriteButtonLabel: UIButton!
    
    var addSongToFavoriteClosure:(() -> Void)?
    var removeSongFromFavoriteClosure:(() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        
        if favoriteButtonLabel.isSelected {
            favoriteButtonLabel.isSelected = false
            removeSongFromFavoriteClosure?()
            
        } else {
            favoriteButtonLabel.isSelected = true
            addSongToFavoriteClosure?()
            
        }
       
    }
}
