//
//  PopUpViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 13.09.2021.
//

import UIKit

class PopUpViewController: UIViewController {

    
    var link: String = ""
    
    @IBOutlet weak var actionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        actionButton.titleLabel?.numberOfLines = 0

    }
    
    @IBAction func CheckOnLastFMClicked(_ sender: Any) {
        
            if let trackLink = URL(string: link){
                UIApplication.shared.open(trackLink, options: [:]) { done in
                    print("пользователь перешёл по ссылке")
                }
            }
        
    }
    
    


}
