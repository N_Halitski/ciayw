//
//  ActivityViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 05.09.2021.
//

import UIKit
import FirebaseAuth

class ActivityViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){ [weak self] in // используем dispatchqueue так как нам нужно подождать 2 секнуды
            guard let self = self else {return}
            if Auth.auth().currentUser?.uid != nil{ // значит я залогинин, переходим на тапбар со экранами
                
                let controller = self.storyboard?.instantiateViewController(identifier: "TabBarController")
                self.navigationController?.pushViewController(controller!, animated: true)
            } else { // иначе на экран welcome где переходим на жкрна регистрации
                let welcomeVC = self.storyboard?.instantiateViewController(identifier: "WelcomeViewController") as! WelcomeViewController
                self.navigationController?.pushViewController(welcomeVC, animated: true)
            }
            
        }
    }

   

}
