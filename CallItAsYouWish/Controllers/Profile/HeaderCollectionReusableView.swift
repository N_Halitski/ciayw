//
//  HeaderCollectionReusableView.swift
//  CallItAsYouWish
//
//  Created by Nikita on 14.07.2021.
//

import UIKit
import Kingfisher

class HeaderCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileUserName: UILabel!
    @IBOutlet weak var typeOfProfile: UILabel!
    @IBOutlet weak var numberOfPostsLabel: UILabel!
    @IBOutlet weak var numberOfFollowingsLabel: UILabel!
    @IBOutlet weak var numberOfFollowersLabel: UILabel!
    let headerId = "HeaderCollectionReusableView"
    
    var posts:[Posts] = []
    
    func configure(with user: UserData) {
        backgroundColor = .darkGray
        if let profileAvatar = user.profilePicture{
            let photo = URL(string: profileAvatar)
            self.profileImage.kf.setImage(with: photo)
        }
        self.profileName.text = user.fullName
        self.profileUserName.text = user.userName
        self.typeOfProfile.text = user.typeOfProfile
        self.numberOfFollowersLabel.text = String((user.followers?.numberOfFollowers)!)
        self.numberOfFollowingsLabel.text = String((user.followings?.numberOfFollowings)!)
        self.numberOfPostsLabel.text = String(posts.count)
        }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
}
