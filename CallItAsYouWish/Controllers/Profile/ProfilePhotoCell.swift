//
//  ProfilePhotoCell.swift
//  CallItAsYouWish
//
//  Created by Nikita on 13.07.2021.
//

import UIKit

class ProfilePhotoCell: UICollectionViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
