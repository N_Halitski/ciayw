//
//  ProfileViewController.swift
//  CallItAsYouWish
//
//  Created by Nikita on 13.06.2021.
//

import UIKit
import Firebase
import Alamofire
import Kingfisher
import RealmSwift



class ProfileViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    var posts: [Posts] = []
    var user: UserData?
    var activityIndicator: UIActivityIndicatorView?
    
    
    @IBOutlet weak var profileGalleryCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let logOutButton = UIButton(type: .system)
        logOutButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        logOutButton.setTitle("Log Out", for: .normal)
        logOutButton.addTarget(self, action: #selector(logOutButtonisClicked), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: logOutButton)
        
        let cellXib = UINib(nibName: "ProfilePhotoCell", bundle: nil)
        profileGalleryCollectionView.register(cellXib, forCellWithReuseIdentifier: "ProfilePhotoCell")
        
        let hXib = UINib(nibName: "HeaderCollectionReusableView", bundle: nil)
        profileGalleryCollectionView.register(hXib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        profileGalleryCollectionView.delegate = self
        profileGalleryCollectionView.dataSource = self
        
        showActivityIndicator()
      
        
        DispatchQueue.global(qos: .background).sync { [weak self] in
            guard let self = self else {return}
            let profilePostsManager = ProfilePostManager()
            profilePostsManager.downloadProfileWithPosts { [weak self] posts in
                guard let self = self else {return}
                self.posts = posts
                print("Колличество постов: \(posts.count)")

                let profileUserDataManager = ProfileUserDataManager()
                profileUserDataManager.downloadProfileFollowers { [weak self] user in
                    guard let self = self else {return}
                    self.user = user

                    DispatchQueue.main.async {
                        self.hideActivityIndicator()
                        self.profileGalleryCollectionView.reloadData()
                    }
                }

            }
        }
        
        self.navigationItem.title = "Profile"
        
    }
    
    @objc func logOutButtonisClicked(){
        print("realm file:\(Realm.Configuration.defaultConfiguration.fileURL?.path)")
        let realm = try! Realm()
        
        let tracks = realm.objects(RealmTracksDB.self)
        let artistForTracks = realm.objects(RealmArtistForTrack.self)
        let imagesForTracks = realm.objects(RealmTrackImage.self)
        let favouriteTracks = realm.objects(RealmFavoriteTrack.self)
        
        let artists = realm.objects(RealmArtistDB.self)
        let imagesForArtists = realm.objects(RealmImageOption.self)
        let favouriteArtists = realm.objects(RealmFavoriteArtist.self)
        
        
        try! realm.write{
            realm.delete(tracks)
            realm.delete(artistForTracks)
            realm.delete(imagesForTracks)
            realm.delete(favouriteTracks)
            
            
            realm.delete(artists)
            realm.delete(imagesForArtists)
            realm.delete(favouriteArtists)
        }
        
        try! Auth.auth().signOut()
                
     tabBarController?.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePhotoCell", for: indexPath) as! ProfilePhotoCell
        let post = posts[indexPath.row]
        if let parsingPost = post.postImage?.imageOption?.photoUrl{
            let photo = URL(string: parsingPost)
            cell.photoImageView.kf.setImage(with: photo)
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let offSet: CGFloat = 20 // отступы слева справав
        let spacing: CGFloat = 10
        let screenWidth = UIScreen.main.bounds.width
        let cellWidth = (screenWidth - offSet*2 - spacing) / 2
        let size = CGSize(width: cellWidth, height: 230)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        10
    }
    
    //расстояние по y(игрику) между элементами
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
        // нужно скастить  as! HeaderCollectionReusableView потому что иначе будет не понимать откуда должна прийти функция configure()
        
        
        header.posts = posts
        
        if let user = user {
            header.configure(with: user)
        }
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: view.frame.size.height / 3.5)
    }
    
    
    func showActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator?.center = self.view.center
        activityIndicator?.color = .systemOrange
        self.view.addSubview(activityIndicator!)
        activityIndicator?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityIndicator != nil){
            activityIndicator?.stopAnimating()
        }
    }
    
}



