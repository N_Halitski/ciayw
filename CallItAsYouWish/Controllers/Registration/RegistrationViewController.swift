//
//  RegistrationViewController.swift
//  Мой Проект
//
//  Created by Nikita on 06.06.2021.
//

import UIKit
import Firebase

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
  
    var offsetBeforeKeyboardDidShown: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem( title: "Registration", style: .plain, target: nil, action: nil) // navigationcontroller с кнопкой на следуюзем экране принадлжежит предыдущему. и если хотим его менять - нужно это делать на том экране, с которого происзодит переход
        
      

    }
    
    
    
    
    
    
    @IBAction func registrationClicked(_ sender: Any) {
        if let email = emailTextField.text, let password = passwordTextField.text {
            //  сервис firebase
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if user != nil {
                    let tabbar: UITabBarController? = (self.storyboard!.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController)
//                    print("navigationController = \(self.navigationController)")
                    self.navigationController?.pushViewController(tabbar!, animated: true)
                    
                    
                } else {
                    let errorMessage = error?.localizedDescription ?? "Error" // если user nil то зайзет сюда
                    let alertVC = UIAlertController(title: nil, message: errorMessage, preferredStyle: .alert) // создали алерт
                    let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil) // создали кнопку ОК
                    
                    alertVC.addAction(action) // добавили кнопку к алерту
                    self.present(alertVC, animated: true, completion: nil)
                }
            }
        }
    }
}





