//
//  FollowersProfileManager.swift
//  CallItAsYouWish
//
//  Created by Nikita on 27.07.2021.
//

import Foundation
import Alamofire

class ProfileUserDataManager {
    func downloadProfileFollowers(onComplition:((Follower)->Void)?)  {
        let url = "https://instagram47.p.rapidapi.com/user_followers?userid=271651512"
        let headers: HTTPHeaders = [
            "x-rapidapi-key": "291b1a506dmsh309da27dbbe77a4p1de577jsne74cc74b56af",
            "x-rapidapi-host": "instagram47.p.rapidapi.com"
        ]
        
        AF.request(url, headers: headers).responseJSON { response in
            print(response.value)
            
            if let responseDictionary = response.value as? [String: Any] {
                if let bodyDict = responseDictionary["body"] as? [String:Any]{
                    var result = Follower()
                    if let numberOfFollowers = bodyDict["count"] as? Int {
                        result.numberOfFollowers = numberOfFollowers
                    }
                    onComplition?(result)
                }
            }
        }
    }
}
