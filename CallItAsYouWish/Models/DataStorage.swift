//
//  DataStorage.swift
//  CallItAsYouWish
//
//  Created by Nikita on 17.08.2021.
//

import Foundation

class DataStorage {
    static let shared = DataStorage()
    
    var favouriteTracks: [Track] = []
    var favouriteArtists: [TopArtist] = []
}
