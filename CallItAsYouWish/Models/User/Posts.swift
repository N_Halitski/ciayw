//
//  Posts.swift
//  CallItAsYouWish
//
//  Created by Nikita on 27.07.2021.
//

import Foundation

struct Posts { // items
    var numberOfLikesInPost:Int?
    var numberOfComments:Int?
    var caption: Caption?
    var postImage: Image?
}


struct Caption {
    var text: String?
}

struct Image {
    var imageOption: PhotoOptions?
}

struct PhotoOptions {
    var photoUrl: String?
}
