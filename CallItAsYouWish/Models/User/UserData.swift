//
//  Followers.swift
//  CallItAsYouWish
//
//  Created by Nikita on 27.07.2021.
//

import Foundation

struct UserData {
    var fullName: String?
    var userName: String?
    var typeOfProfile: String?
    var profilePicture: String?
    var followers: Followers?
    var followings: Followings?
    
}

struct Followers {
    var numberOfFollowers: Int?
}

struct Followings {
    var numberOfFollowings: Int?
}
