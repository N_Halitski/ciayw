//
//  RealmFavorites.swift
//  CallItAsYouWish
//
//  Created by Nikita on 01.09.2021.
//

import Foundation
import RealmSwift
 

class RealmFavoriteTrack: Object {
    @Persisted(primaryKey: true) var id: String = ""
}

class RealmFavoriteArtist: Object {
    @Persisted(primaryKey: true) var id: String = ""
}
