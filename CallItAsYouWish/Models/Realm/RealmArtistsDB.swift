//
//  RealmArtistDB.swift
//  CallItAsYouWish
//
//  Created by Nikita on 24.08.2021.
//

import Foundation
import RealmSwift

class RealmArtistDB: Object {
    @objc @Persisted (primaryKey: true) var id: String = "" //  @objc для того, чтобы можно  использовать keyPath по фильтрации
    @Persisted var name: String = ""
    @Persisted var numberOfPlay: String = ""
    @Persisted var numberOfListeners: String = ""
    @Persisted var url: String = ""
    @Persisted var images = List<RealmImageOption>()
    @Persisted var userId: String = ""

}

class RealmImageOption: Object{
    @Persisted var url: String?
}
