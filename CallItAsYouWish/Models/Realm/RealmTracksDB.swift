

import Foundation
import RealmSwift

class RealmTracksDB: Object {
    @objc @Persisted (primaryKey: true) var id: String = ""
    @Persisted var name: String = ""
    @Persisted var playCounts: String = ""
    @Persisted var numberOfListeners: String = ""
    @Persisted var artist: RealmArtistForTrack?
    @Persisted var images: RealmTrackImage?
    @Persisted var songLink: String = ""
    @Persisted var userId: String = ""
}



class RealmArtistForTrack: Object {
    @Persisted var name: String = ""
    @Persisted var url: String = ""
}


class RealmTrackImage: Object {
    @Persisted var smallImage: String = ""
    @Persisted var mediumImage: String = ""
}
