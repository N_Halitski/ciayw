//
//  TrackImage.swift
//  CallItAsYouWish
//
//  Created by Nikita on 02.08.2021.
//

import Foundation

struct TrackImage {
    var smallImage: String?
    var mediumImage: String?
}
