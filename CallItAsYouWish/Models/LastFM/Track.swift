//
//  Track.swift
//  CallItAsYouWish
//
//  Created by Nikita on 02.08.2021.
//

import Foundation

struct Track {
    var id: String = ""
    var name: String?
    var playCounts: String?
    var numberOfListeners: String?
    var artist: Artist?
    var image: TrackImage?
    var songLink: String?
  }
