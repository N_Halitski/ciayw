//
//  TopArtist.swift
//  CallItAsYouWish
//
//  Created by Nikita on 03.08.2021.
//

import Foundation
struct TopArtist {
    var id: String = ""
    var name: String?
    var numberOfPlay: String?
    var numberOfListeners: String?
    var url: String?
    var image: [ImageOption]?
    
    
}
