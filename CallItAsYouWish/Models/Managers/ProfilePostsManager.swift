//
//  ProfilePostsManager.swift
//  CallItAsYouWish
//
//  Created by Nikita on 22.07.2021.
//

import Foundation

class ProfilePostManager {
    func downloadProfileWithPosts(onComplite: (([Posts])-> Void)?){
        
        let headers = [
            "x-rapidapi-key": "8127e93ddamsha49b9fdd600006cp16ee2ajsn50d1c426bef5",
            "x-rapidapi-host": "instagram47.p.rapidapi.com"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://instagram47.p.rapidapi.com/user_posts?username=niki_hal")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                do{
                    let response = try JSONSerialization.jsonObject(with: data!, options: []) as? [String : Any]
                    print("Дата: \(response)")
                    
                    if let responseDictionary = response {
                        var result:[Posts] = []
                        if let bodyDict = responseDictionary["body"] as? [String: Any] {
                            if let itemsDict = bodyDict["items"] as? [[String: Any]] {
                                for itemDict in itemsDict {
                                    var post = Posts()
                                    post.postImage = Image()
                                    post.postImage?.imageOption = PhotoOptions()
                                    post.numberOfLikesInPost = itemDict["like_count"] as? Int
                                    post.numberOfComments = itemDict["comment_count"] as? Int
                                    
                                    if let captionDict = itemDict["caption"] as? [String: Any] {
                                        var caption = Caption()
                                        caption.text = captionDict["text"] as? String
                                        
                                        post.caption = caption
                                    }
                                    if let postImageDict = itemDict["image_versions2"] as? [String: Any] {
                                        if let imagesDict = postImageDict["candidates"] as? [[String: Any]]{
                                            var imageUrl = imagesDict[1]
                                            if let photoUrl = imageUrl["url"] as? String{
                                                var photo = PhotoOptions()
                                                photo.photoUrl = photoUrl
        
                                                post.postImage?.imageOption?.photoUrl = photoUrl
                                            }
                                        }
                                    }
                                    result.append(post)
                                }
                            }
                        }
                        onComplite?(result)
                    }
                    
                }catch{ print("erroMsg") }
                
                let httpResponse = response as? HTTPURLResponse
                
            }
        })
        
        dataTask.resume()
     
    }
}
