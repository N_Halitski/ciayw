//
//  ServerManager.swift
//  CallItAsYouWish
//
//  Created by Nikita on 05.09.2021.
//

import Foundation
import FirebaseDatabase
import RealmSwift

class ServerManager{
    
    static let shared = ServerManager()
    
    private let reference = Database.database().reference()
    
    
    func sendTrackToFirebase(_ realmTrack: RealmTracksDB){
        let json = ["id": realmTrack.id,
                    "userId": realmTrack.userId,
                    "name": realmTrack.name,
                    "artistName": realmTrack.artist?.name,
                    "image": realmTrack.images?.smallImage,
                    "songLink": realmTrack.songLink
        ]
        let child = reference.child("tracks").child("\(realmTrack.id)")
        child.setValue(json) { error, ref in
            
        }
    }
    
    
    
    func downloadTracksFromFirebase(for userId: String){
        let database = Database.database().reference()
        // забиарем все id песен по ключу юзер айдт лежит конкретная строка
        // в папке tracks в выберет те строки которые относится к этому юхер айди которым мы залогинились
        let child = database.child("tracks").queryOrdered(byChild: "userId").queryEqual(toValue: userId)
        child.observeSingleEvent(of: .value) { response in
            if let value = response.value as? [String: Any]{
                for item in value.values{
                    if let trackJson = item as? [String:Any]{
                        if let artistName = trackJson["artistName"] as? String,
                           let id = trackJson["id"] as? String,
                           let image = trackJson["image"] as? String,
                           let name = trackJson["name"] as? String,
                           let songLink = trackJson["songLink"] as? String,
                           let userId = trackJson["userId"] as? String{
                            
                            let realmTrack = RealmTracksDB()
                            
                            realmTrack.artist = RealmArtistForTrack()
                            realmTrack.artist?.name = artistName
                            realmTrack.id = id
                            
                            
                            
                            realmTrack.images = RealmTrackImage()
                            realmTrack.images?.smallImage = image
                            realmTrack.name = name
                            realmTrack.songLink = songLink
                            realmTrack.userId = userId
                            
                            // отдельно добавляю отметку для избранного трека так как TopTracksVC  есть проверка на избранность в методе cellForRow at
                            let favouriteTrack = RealmFavoriteTrack()
                            favouriteTrack.id = realmTrack.id
                            
                            let realm = try! Realm()
                            try! realm.write {
                                realm.add(favouriteTrack)
                                realm.add(realmTrack, update: .all)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func sendArtistToFirebase(_ realmArtist: RealmArtistDB){
        let json = ["id": realmArtist.id,
                    "name":realmArtist.name,
                    "userId": realmArtist.userId,
                    "numberOfPlay": realmArtist.numberOfPlay,
                    "image": realmArtist.images[0].url,
                    "url": realmArtist.url
        ]
        let child = reference.child("artists").child("\(realmArtist.id)")
        child.setValue(json) { error, ref in
            
        }
        
    }
    
    func downloadArtistsFromFirebase(for userId: String){
        let database = Database.database().reference()
        // забиарем все id песен по ключу юзер айдт лежит конкретная строка
        // в папке artists в выберет те строки которые относится к этому юхер айди которым мы залогинились
        let child = database.child("artists").queryOrdered(byChild: "userId").queryEqual(toValue: userId)
        child.observeSingleEvent(of: .value) { response in
            if let value = response.value as? [String: Any]{
                for item in value.values{
                    if let artistJson = item as? [String:Any]{
                        if let id = artistJson ["id"] as? String,
                           let image = artistJson ["image"] as? String,
                           let name = artistJson ["name"] as? String,
                           let numberOfPlay = artistJson ["numberOfPlay"] as? String,
                           let url = artistJson ["url"] as? String,
                           let userId = artistJson["userId"] as? String{
                            
                            let realmArtist = RealmArtistDB()
                            realmArtist.id = id
                            
                            let realmImage = RealmImageOption()
                            realmImage.url = image
                            realmArtist.images.append(realmImage)
                            realmArtist.name = name
                            realmArtist.numberOfPlay = numberOfPlay
                            realmArtist.url = url
                            realmArtist.userId = userId
                            
                            // отдельно добавляю отметку для избранного артиста так как TopArtistsVC  есть проверка на избранность в методе cellForRow at
                            let favouriteArtist = RealmFavoriteArtist()
                            favouriteArtist.id = realmArtist.id
                            
                            let realm = try! Realm()
                            try! realm.write {
                                realm.add(favouriteArtist)
                                realm.add(realmArtist, update: .all)
                            }
                            
                        }
                    }
                }
            }
        }
    }
    
    func removeTrackFromFirebase(for id: String) {
        let child = reference.child("tracks").child("\(id)")
        child.setValue(nil)
    }
    
    func removeArtistFromFirebse (for id: String){
        
        let child = reference.child("artists").child("\(id)")
        child.setValue(nil)
    }
}
