//
//  LastFMManager.swift
//  CallItAsYouWish
//
//  Created by Nikita on 02.08.2021.
//

import Foundation
import FirebaseAuth
import Alamofire

class LastFMManager{
    let baseUrl = "https://ws.audioscrobbler.com/2.0/"
    let apiKey = "497f10a85faba08d9cfda70664b8e262"
    let format = "json"
    
    var userId = Auth.auth().currentUser?.uid
    let getTopTracks = "chart.gettoptracks"
    let getTopArtist = "chart.gettopartists"
    
    
    func getTopTracks (onCompletion:@escaping ([Track])->Void){
        
        let url = URL.init(string: baseUrl)!
        let parameters = ["method": getTopTracks,
                          "api_key": apiKey,
                          "format": format]
        AF.request(url, method: .get, parameters: parameters).responseJSON { (response) in
            print(response)
            
            if let responseDictionary = response.value as? [String:Any]{
                var result: [Track] = []
                if let tracksDict = responseDictionary["tracks"] as? [String:Any]{
                    if let trackDict = tracksDict["track"] as?[[String:Any]]{
                        for oneTrack in trackDict{
                            var track = Track()
                            track.artist = Artist()
                            track.image = TrackImage()
                            if let artistDict = oneTrack["artist"] as? [String: Any]{
                                var artist = Artist()
                                artist.name = artistDict["name"] as? String
                                artist.url = artistDict["url"] as? String
                                track.artist = artist
                            }
                            if let imagesDict = oneTrack["image"] as? [[String:Any]]{
                                var smallImageUrl = imagesDict[0]
                                var mediumImageUrl = imagesDict[1]
                                if let smallImageURL = smallImageUrl["#text"] as? String,
                                   let mediumImageURL = mediumImageUrl["#text"] as? String{
                                    var image = TrackImage()
                                    image.smallImage = smallImageURL
                                    image.mediumImage = mediumImageURL
                                    
                                    track.image = image
                                }
                            }
                            if let numberOfListeners = oneTrack["listeners"] as? String{
                                track.numberOfListeners = numberOfListeners
                            }
                            if let name = oneTrack["name"] as? String{
                                track.name = name
                            }
                            if let playCount = oneTrack["playcount"] as? String{
                                track.playCounts = playCount
                            }
                            if let songUrl = oneTrack["url"] as? String{
                                track.songLink = songUrl
                            }
                            
                            track.id = track.name! + track.artist!.name! + self.userId!
                            result.append(track)
                        }
                    }
                }
                onCompletion(result)
            }
        }
    }
    
    
    func getTopArtists (onCompletion:@escaping ([TopArtist])->Void){
        
        let url = URL.init(string: baseUrl)!
        let parameters = ["method": getTopArtist,
                          "api_key": apiKey,
                          "format": format]
        
        AF.request(url, method: .get, parameters: parameters).responseJSON { response in
            
                if let responseDictionary = response.value as? [String:Any]{
                var result:[TopArtist] = []
                if let artistsDict = responseDictionary["artists"] as? [String:Any]{
                    if let artistDict = artistsDict["artist"] as? [[String:Any]]{
                        for oneArtist in artistDict{
                            var artist = TopArtist()
                            artist.image = [ImageOption]()
                            if let imageDict = oneArtist["image"] as? [[String:Any]]{
                                for imageOption in imageDict{
                                    var image = ImageOption()
                                    image.url = imageOption["#text"] as? String
                                    artist.image?.append(image)
                                }
                            }
                            if let listeners = oneArtist["listeners"] as? String{
                                artist.numberOfListeners = listeners
                            }
                            if let name = oneArtist["name"] as? String{
                                artist.name = name
                            }
                            if let playcount = oneArtist["playcount"] as? String{
                                artist.numberOfPlay = playcount
                            }
                            if let url = oneArtist["url"] as? String{
                                artist.url = url
                            }
                            artist.id = artist.name! + self.userId!
                            result.append(artist)
                        }
                    }
                }
                onCompletion(result)
            }
        }
    }

}
