//
//  FollowersProfileManager.swift
//  CallItAsYouWish
//
//  Created by Nikita on 27.07.2021.
//

import Foundation
import Alamofire

class ProfileUserDataManager {
    func downloadProfileFollowers(onComplition:((UserData)->Void)?)  {
        let url = "https://instagram28.p.rapidapi.com/user_info?user_name=niki_hal"
        let headers: HTTPHeaders = [
            "x-rapidapi-key": "8127e93ddamsha49b9fdd600006cp16ee2ajsn50d1c426bef5",
            "x-rapidapi-host": "instagram28.p.rapidapi.com"
        ]
        
        AF.request(url, headers: headers).responseJSON { response in
            print(response.value)
            
            if let responseDictionary = response.value as? [String: Any]{
                if let dataDict = responseDictionary["data"] as? [String:Any]{
                    if let userDict = dataDict["user"] as? [String:Any]{
                        var result = UserData()
                        
                        if let followersJson = userDict["edge_followed_by"] as? [String:Any]{
                            var followers = Followers()
                            if let numberOfFollowers = followersJson["count"] as? Int{
                                followers.numberOfFollowers = numberOfFollowers
                            }
                            result.followers = followers
                        }
                        
                        if let followingsJson = userDict["edge_follow"] as? [String:Any]{
                            var followings = Followings()
                            if let numberOfFollowings = followingsJson["count"] as? Int{
                                followings.numberOfFollowings = numberOfFollowings
                            }
                            result.followings = followings
                        }
                        
                        if let fullName = userDict["full_name"] as? String{
                            result.fullName = fullName
                        }
                        
                        if let categoryName = userDict["category_name"] as? String{
                            result.typeOfProfile = categoryName
                        }
                        
                        if let profilePhoto = userDict["profile_pic_url"] as? String{
                            result.profilePicture = profilePhoto
                        }
                        if let userName = userDict["username"] as? String{
                            result.userName = userName
                        }
                        
                        
                       
                        onComplition?(result)
                    }
                }
            }
        }
        
        
        
        
        //        AF.request(url, headers: headers).responseJSON { response in
        //            print(response.value)
        //
        //            if let responseDictionary = response.value as? [String: Any] {
        //                if let bodyDict = responseDictionary["body"] as? [String:Any]{
        //                    var result = UserData()
        //                    if let numberOfFollowers = bodyDict["count"] as? Int {
        //                        result.numberOfFollowers = numberOfFollowers
        //                    }
        //                    onComplition?(result)
        //                }
        //            }
        //        }
    }
}
